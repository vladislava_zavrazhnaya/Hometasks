/*1)
Перепишите if с использованием оператора "?"
if (a + b) < 4 {
    result = "Мало";
} else {
    result = "Много";
}; */

let a = parseInt(prompt("Введите первое число.", 0));/*Задаём значение переменных.*/


let b = parseInt(prompt("Введите второе число.", 0));


let result = undefined;/*Можем не указывать "undefined", а оставить "let result;"*/


if (((a + b) < 4) ? result = "Мало" : result = "Много");


document.write(result);


document.write("<hr/>");


/*2)
Перепишите if...else с использованием нескольких операторов "?"
var message;*/

let login = prompt("Введите логин:", "Вася", "Директор");

let message;

if (login == "Вася" ? message = "Привет" : "");


if (login == "Директор" ? message = "Здравствуйте" : "");


if (login == "" ? message = "Нет логина" : "");


document.write(login + " " + message);

