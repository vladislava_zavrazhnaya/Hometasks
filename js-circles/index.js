const buttonPaint = document.querySelector(".btn")
buttonPaint.onclick = () => {
    let diametr = parseInt(prompt('Put the diametr here:'));
    if (diametr > 0) {
        for (let i = 0; i < 100; i++) {
            let around = document.querySelector(".container");
            let circle = document.createElement("input");
            const color = ["red", "orange", "yellow", "green", "blue", "purple", "white", "black", "pink", "brown"];
            let newColor = color[Math.floor(Math.random() * 10)];
            circle.setAttribute("type", "button");
            circle.setAttribute("value", `    `);
            circle.setAttribute("onclick", "deleteSelf(this)");
            circle.style.backgroundColor = `${newColor}`;
            circle.style.padding = `${diametr}px`;
            circle.classList.add("circles")
            around.append(circle);
        };
    }
};
const deleteSelf = (button) => {
    button.remove();

};
